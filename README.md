# android-tools

Webcam apps such as Iriun require `adb` on the computer for USB-only connection
(which usually has less lag than Wifi) to work.

`fastboot` and `adb` are necessary for flashing devices, and the platform tools
are *required* to set the right udev rules on the computer.


## udev rules

> The `android-sdk-platform-tools-common` package contains a community-maintained
> default set of udev rules for Android devices.
> But they do not cover as many devices as the udev rules maintained
> by [Gianluca Boiano](https://github.com/M0Rf30/android-udev-rules).

+ https://github.com/M0Rf30/android-udev-rules
+ https://askubuntu.com/questions/1169509/user-in-plugdev-group-are-your-udev-rules-wrong-or-error-insufficient-permissi
+ https://stackoverflow.com/questions/53887322/adb-devices-no-permissions-user-in-plugdev-group-are-your-udev-rules-wrong


## Remote ADB server

> To connect to a remote device, it is possible to connect a local `adb` client
> to a remote `adb` server provided they use the same version of the `adb` protocol.
> https://github.com/Genymobile/scrcpy/blob/master/doc/tunnels.md

In my case, the client is an amd64 workstation running Ubuntu Jammy running
```
$ adb --version
Android Debug Bridge version 1.0.41
Version 28.0.2-debian
Installed as /usr/lib/android-sdk/platform-tools/adb
```
and the server is a Raspberry Pi 3B (armhf) running Ubuntu Jammy with:
```
$ adb --version
Android Debug Bridge version 1.0.41
Version 29.0.6-debian
Installed as /usr/lib/android-sdk/platform-tools/adb
```
Luckily it appears both use the same ADB version (1.0.41).
No need to attempt pinning apt packages or building from source, etc.

Note that my use case is the following:

+ I have Android devices connected to the client, while
+ simultaneously having Android devices connected to the server.

Therefore I am not looking to
[hijack the client's ADB server](https://stackoverflow.com/a/71268643/1198249)
to the server's by binding port `localhost:5037` to `5037` on the remote server,
since that would render any locally connected Android devices unaccessible.

+ https://github.com/jarhot1992/Remote-ADB/blob/main/md/tutorials.md
+ https://stackoverflow.com/questions/13278429/how-to-set-up-adb-for-remote-machine-development-and-local-device-deployment
+ https://medium.com/@enriqueramrezmanzaneda/using-your-raspberry-pi-to-manage-your-home-android-devices-for-development-48aca6840214
  round-about method to connect to Android device via Wifi
+ https://medium.com/@RamanFromIndia/how-to-use-adb-from-remote-machine-with-a-local-physical-device-c5587875be02
  simply hijacks the local 5037 port, and no mention of the 27183 port
+ http://bresilla.com/post/hardware/raspberry/usbip Interesting, a different
  approach - make the remote USB port behave as if it was connected locally with `usbip`.
  I think this could be very useful for a permanently attached Android device, but less
  so for my current use case where devices will come and go occasionally.
+ https://bitbucket.org/chabernac/adbportforward/wiki/Home
  uses the less secure approach of listening to all interfaces, which I don't see
  the point of given the availability of SSH tunnels and VPN mesh networks
+ https://stackoverflow.com/questions/1754162/remote-debugging-with-android-emulator
+ https://old.reddit.com/r/tasker/comments/di4ye7/help_using_adb_from_a_raspberry_pi
  setup could likely work even on Raspberry Pi Zero


## Links and notes

+ https://developer.android.com/studio/run/device
+ https://developer.android.com/studio/debug/dev-options
+ https://developer.android.com/tools/variables
+ https://github.com/Linuxndroid/ADB-Cheat-Sheet
+ https://github.com/mzlogin/awesome-adb/blob/master/README.en.md
+ https://xdaforums.com/t/guide-linux-installing-adb-and-fastboot-on-linux-device-detection-drivers.3478678


### Control and manage Android devices from your browser (self-hosted)

+ https://github.com/DeviceFarmer/stf (much more than my needs at present, but good to know)
